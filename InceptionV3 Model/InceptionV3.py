import os
import sys
import glob
import argparse
import matplotlib.pyplot as plt
from keras.applications.inception_v3 import InceptionV3, preprocess_input
from keras.models import Model,Sequential
from keras.layers import Dense, GlobalAveragePooling2D, Dropout
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import sgd


img_height, img_width = 299, 299 #fixed size for InceptionV3
num_layers_to_freeze = 305


def get_nb_files(directory):
    """Get number of files by searching directory recursively"""
    if not os.path.exists(directory):
        return 0
    count = 0
    for i, dirs, files in os.walk(directory):
        for j in dirs:
            count += len(glob.glob(os.path.join(i, j + "/*")))
    return count
    
def setup_to_transfer_learn(model, base_model):
    """Freeze all layers and compile the model"""
    for layer in base_model.layers:
        layer.trainable = False
    
    model.compile(optimizer='SGD', loss='categorical_crossentropy', metrics=['accuracy'])

def add_new_last_layer(base_model, num_classes):
    
    """Add last layer to the convnet
    Args:
    base_model: keras model excluding top
    nb_classes: # of classes
    Returns:
    new keras model with last layer
    """
    model_top = Sequential()
    model_top.add(GlobalAveragePooling2D(input_shape=base_model.output_shape[1:], data_format=None)),  
    model_top.add(Dense(256, activation='relu'))
    model_top.add(Dropout(0.5))
    model_top.add(Dense(8, activation='softmax')) 
    model = Model(inputs=base_model.input, outputs=model_top(base_model.output)) 
    return  model

def setup_to_finetune(model):
    """
    Freeze the bottom num_layers_to_freeze and retrain the remaining top layer.
    note: num_layers_to_freeze corresponds to the top inception block in the inceptionv3 arch
    Args:
    model: keras model
    """
    for layer in model.layers[:num_layers_to_freeze]:
        layer.trainable = False
    for layer in model.layers[num_layers_to_freeze:]:
        layer.trainable = True    
    model.compile(optimizer=sgd(lr=2e-4,momentum=0.9, decay=0), loss='categorical_crossentropy', metrics=['accuracy'])

def train(args):
    """Use transfer learning and fine-tuning to train a network on a new dataset"""
    nb_train_samples = get_nb_files(args.train_dir)
    nb_classes = len(glob.glob(args.train_dir + "/*"))
    nb_val_samples = get_nb_files(args.val_dir)
    nb_epoch = int(args.nb_epoch)
    batch_size = int(args.batch_size)

    # data prep
    train_datagen =  ImageDataGenerator(
       preprocessing_function=preprocess_input,
       rescale=1./255, 
       rotation_range=30,
       width_shift_range=0.2,
       height_shift_range=0.2,
       shear_range=0.2,
       zoom_range=0.2,
       horizontal_flip=True
    )
    validation_datagen = ImageDataGenerator(
       preprocessing_function=preprocess_input,
       rescale=1./255, 
       rotation_range=30,
       width_shift_range=0.2,
       height_shift_range=0.2,
       shear_range=0.2,
       zoom_range=0.2,
       horizontal_flip=True
    )

    train_generator = train_datagen.flow_from_directory(
     args.train_dir,
     target_size=(img_height, img_width),
     batch_size=batch_size,
    )

    validation_generator = validation_datagen.flow_from_directory(
            args.val_dir,
            target_size=(img_height, img_width),
            batch_size=batch_size,
    )
  
    # setup model
    base_model = InceptionV3(weights='imagenet', include_top=False) #include_top=False excludes final FC layer
    model = add_new_last_layer(base_model, nb_classes)

    # transfer learning
    setup_to_transfer_learn(model, base_model)

    V3_model = model.fit_generator(
            train_generator,
            nb_epoch=20,
            samples_per_epoch=nb_train_samples//train_generator.batch_size,
            validation_data=validation_generator,
            nb_val_samples=nb_val_samples//validation_generator.batch_size,verbose=1
    )
    
    # fine-tuning
    setup_to_finetune(model)

    V3_model_ft = model.fit_generator(
            train_generator,
            samples_per_epoch=nb_train_samples//train_generator.batch_size,
            nb_epoch=nb_epoch,
            validation_data=validation_generator,
            nb_val_samples=nb_val_samples//validation_generator.batch_size,verbose=1
    )

    model.save(args.V3_model)

    if args.plot:
        plot_training(V3_model_ft)
        
def plot_training(history):
    acc = history.history['acc']
    val_acc = history.history['val_acc']
    loss = history.history['loss']
    val_loss = history.history['val_loss']
    epochs = range(len(acc))

    plt.plot(epochs, acc, 'r.')
    plt.plot(epochs, val_acc, 'r')
    plt.title('Training and validation accuracy')

    plt.figure()
    plt.plot(epochs, loss, 'r.')
    plt.plot(epochs, val_loss, 'r-')
    plt.title('Training and validation loss')
    plt.show()


if __name__=="__main__":
  a = argparse.ArgumentParser()
  a.add_argument("--train_dir")
  a.add_argument("--val_dir")
  a.add_argument("--nb_epoch", default=20)
  a.add_argument("--batch_size", default=20)
  a.add_argument("--output_model_file", default="inceptionv3-ft.model")
  a.add_argument("--plot", action="store_true")

  args = a.parse_args()
  if args.train_dir is None or args.val_dir is None:
    a.print_help()
    sys.exit(1)

  if (not os.path.exists(args.train_dir)) or (not os.path.exists(args.val_dir)):
    print("directories do not exist")
    sys.exit(1)

  train(args)

